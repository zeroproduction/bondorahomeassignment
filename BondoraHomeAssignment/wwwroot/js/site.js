﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


$(document).ready(function () {

    var table = $("#equipments_table");
    var submitOrder = $("#submit_order");
    var messagesContainer = $(".messages");

    var orderList = [];

    var retrievedOrders = null;

    function buildTableRow(item, i) {

        var td = "<td>" + item.name + "</td>";
        td += "<td>" + item.type_string + "</td>";
        td += "<td><div class=\"col-sm-4 col-md-4\"><input type='text' class='custom-table-field form-control' /></div></td>";
        td += "<td><button ref='" + i + "' class='btn btn-primary custom_table_btn'>Add to Cart</button></td>";

        table.append("<tr>" + td + "</tr>");
    }

    function loadEquipmentTableContent() {
        $.ajax({
            contentType: 'application/json',
            type: "GET",
            async: true,
            url: window.location.href + "api/Equipment",
            success: function (data) {

                setTimeout(function() {
                    console.log(data);

                    retrievedOrders = data.equipments.equipments_list;
                    data.equipments.equipments_list.forEach(function (item, i) {
                        buildTableRow(item, i);
                    });
                }, 200);

            },
            failed: function (data) {
                console.log(data);
            }
        });
    }

    $(document).on("click", ".custom_table_btn", function (ev) {
        ev.preventDefault();

        var $this = $(this);
        var inpValue = $this.parent().parent().find("input").val();

        if (inpValue == 0) return;

        if (inpValue === "" || inpValue === null || inpValue == "undefined") {
            orderList.forEach(function (item, i) {
                if (retrievedOrders[$this.attr("ref")] in orderList) return;
                orderList.splice(i, 1);
                console.log(orderList);
            });
            return;
        }

        orderList.push({
            equipment: retrievedOrders[$(this).attr("ref")],
            date: $(this).parent().parent().find("input").val()
        });

        console.log(orderList);

    });

    loadEquipmentTableContent();

    submitOrder.on("click",
        function (ev) {
            var $this = $(this);
            $this.prop('disabled', true);
            ev.preventDefault();
            $.ajax({
                contentType: 'application/json',
                type: "POST",
                async: true,
                url: window.location.href + "api/Orders",
                dataType: "json",
                data: JSON.stringify({
                    orders_list: orderList,
                }),
                success: function (data) {
                    console.log(data);
                    $this.prop('disabled', false);
                    messagesContainer.html("<div class='alert alert-success'>Successfully added!</div>");
                },
                failed: function (data) {
                    console.log(data);
                    $this.prop('disabled', false);
                    messagesContainer.html("<div class='alert alert-warning'>Error occurred...</div>");
                }
            });
        });
});