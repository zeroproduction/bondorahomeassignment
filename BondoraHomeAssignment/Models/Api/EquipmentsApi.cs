﻿using BondoraHomeAssignment.Server.Models.Equipment;
using Newtonsoft.Json;

namespace BondoraHomeAssignment.Models.Api
{
    public class EquipmentsApi
    {
        [JsonProperty("equipments")]
        public Equipments EquipmentsModel { get; set; }
    }
}
