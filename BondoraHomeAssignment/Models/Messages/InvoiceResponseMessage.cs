﻿using Newtonsoft.Json;

namespace BondoraHomeAssignment.Models.Messages
{
    public class InvoiceResponseMessage
    {
        [JsonProperty("invoice_created")]
        public bool InvoiceCreated { get; set; }
    }
}
