﻿using BondoraHomeAssignment.Models.Messages;
using BondoraHomeAssignment.Server;
using BondoraHomeAssignment.Server.Interfaces;
using BondoraHomeAssignment.Server.Models.Order;
using Microsoft.AspNetCore.Mvc;

namespace BondoraHomeAssignment.Controllers.Api
{
    [Route("api/orders")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly ILoyaltyPointService _loyalityPointService;
        private readonly IInvoiceService _invoiceService;
        private readonly ITextFileService _textFileService;
        private readonly ICacheService _cacheService;

        private void _invoiceService_CreateInvoiceText(object sender, Server.Models.Invoice.Invoice invoice) => _textFileService.SaveToTextFile(invoice);
        private void _orderService_CalculateLoyaltyPoints(object sender, Orders order) => _loyalityPointService.CalculateLoyaltyPoints(order);
        private void _invoiceService_CalculateTotals(object sender, Orders order) => _orderService.CalculateOrderTotals(order);
        private void _orderService_UpdateCustomerOrders(object sender, Orders order) => _cacheService.UpdateCustomerOrders(order, Constants.CustomerId);
        private void _loyalityPointService_UpdateCustomerLoyaltyPoints(object sender, Server.Models.Loyalty.LoyaltyPoint loyaltyPoint)
        {
            _cacheService.UpdateCustomerLoyalityPoints(loyaltyPoint, Constants.CustomerId);
        }

        public OrdersController(IOrderService orderService, ILoyaltyPointService loyalityPointService, 
            IInvoiceService invoiceService, ITextFileService textFileService, ICacheService cacheService)
        {
            _orderService = orderService;
            _loyalityPointService = loyalityPointService;
            _invoiceService = invoiceService;
            _textFileService = textFileService;
            _cacheService = cacheService;
            
            _invoiceService.CalculateLoyaltyPoints += _orderService_CalculateLoyaltyPoints;
            _invoiceService.CreateInvoiceText += _invoiceService_CreateInvoiceText;
            _invoiceService.CalculateTotals += _invoiceService_CalculateTotals;
            _loyalityPointService.UpdateCustomerLoyaltyPoints += _loyalityPointService_UpdateCustomerLoyaltyPoints;
            _orderService.UpdateCustomerOrders += _orderService_UpdateCustomerOrders;
        }

        // POST: api/Orders
        [HttpPost]
        public JsonResult Post([FromBody] Orders orders)
        {
            return new JsonResult(new InvoiceResponseMessage()
            {
                InvoiceCreated = _invoiceService.GenerateInvoice(orders)
            });
        }
    }
}
