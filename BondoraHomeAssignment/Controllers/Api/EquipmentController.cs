﻿using BondoraHomeAssignment.Models.Api;
using BondoraHomeAssignment.Server.Mock;
using Microsoft.AspNetCore.Mvc;

namespace BondoraHomeAssignment.Controllers.Api
{
    [Route("api/equipment")]
    [ApiController]
    public class EquipmentController : ControllerBase
    {
        // Get: api/Equipment
        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult(new EquipmentsApi()
            {
                EquipmentsModel = EquipmentListMockHelper.GetEquipmentList()
            });
        }
    }
}
