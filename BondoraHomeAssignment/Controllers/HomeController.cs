﻿using BondoraHomeAssignment.Server.Interfaces;
using BondoraHomeAssignment.Server.Models.Customer;
using Microsoft.AspNetCore.Mvc;

namespace BondoraHomeAssignment.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(ICacheService cacheService)
        {
            cacheService.SaveCustomerToCache(new Customer());
        }

        public IActionResult Index()
        {

            return View();
        }
    }
}
