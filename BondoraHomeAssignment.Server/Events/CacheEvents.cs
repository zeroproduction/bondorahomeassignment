﻿using BondoraHomeAssignment.Server.Models.Loyalty;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Events
{
    public class CacheEvents
    {
        public delegate void UpdateCustomerLoyaltyPointsEventHandler(object sender, LoyaltyPoint loyaltyPoint);
        public delegate void UpdateCustomerOrdersEventHandler(object sender, Orders order);
    }
}
