﻿using BondoraHomeAssignment.Server.Models.Invoice;

namespace BondoraHomeAssignment.Server.Events
{
    public class CreateInvoiceTextFileEvent
    {
        public delegate void CreateInvoiceTextEventHandler(object sender, Invoice invoice);
    }
}
