﻿using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Events
{
    public class GenerateInvoiceEvents
    {
        public delegate void GenerateInvoiceEventHandler(object sender, Orders order);
    }
}
