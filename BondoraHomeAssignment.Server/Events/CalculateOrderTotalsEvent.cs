﻿using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Events
{
    public class CalculateOrderTotalsEvent
    {
        public delegate void CalculateTotalsEventHandler(object sender, Orders order);
    }
}
