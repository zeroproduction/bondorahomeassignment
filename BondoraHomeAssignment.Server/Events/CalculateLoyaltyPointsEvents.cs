﻿using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Events
{
    public class CalculateLoyaltyPointsEvents
    {
        public delegate void CalculateLoyaltyPointsEventHandler(object sender, Orders order);
    }
}
