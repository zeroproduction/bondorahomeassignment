﻿namespace BondoraHomeAssignment.Server
{
    public class Constants
    {
        public static readonly string InvoiceTextFile = "invoice.txt";
        public static readonly string InvoiceTitle = "Invoice";
        public static readonly int CustomerId = 1;
        public static readonly string CachCustomerConstants = "cache-save-customer";
    }
}
