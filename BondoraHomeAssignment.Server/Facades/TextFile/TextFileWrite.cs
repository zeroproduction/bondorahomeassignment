﻿using System.IO;

namespace BondoraHomeAssignment.Server.Facades.TextFile
{
    public class TextFileWrite
    {
        public void CreateFileWithContent(Models.Invoice.Invoice invoice)
        {
            using (var file = new StreamWriter(Constants.InvoiceTextFile))
            {
                file.WriteLine(invoice.ToString());
                file.Close();
            }
        }
    }
}
