﻿using BondoraHomeAssignment.Server.Facades.Orders;

namespace BondoraHomeAssignment.Server.Facades
{
    public class OrderFacade
    {
        public static CalculateOrdersTotal CalculateOrdersTotal(Models.Order.Orders orders) => new CalculateOrdersTotal(orders);
    }
}
