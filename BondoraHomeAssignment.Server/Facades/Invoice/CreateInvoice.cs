﻿using BondoraHomeAssignment.Server.Models.Loyalty;

namespace BondoraHomeAssignment.Server.Facades.Invoice
{
    public class CreateInvoice
    {
        private readonly LoyaltyPoint _loyaltyPoint;
        private readonly Models.Order.Orders _orders;
        private readonly string _invoiceTitle = Constants.InvoiceTitle;

        public CreateInvoice(Models.Order.Orders order, LoyaltyPoint loyaltyPoint)
        {
            _orders = order;
            _loyaltyPoint = loyaltyPoint;
        }

        public Models.Invoice.Invoice Generate()
        {

            return new Models.Invoice.Invoice()
            {
                Orders = _orders,
                Title = _invoiceTitle,
                TotalPrice = _orders.Total,
                LoyaltyPoint = _loyaltyPoint
            };
        }
    }
}
