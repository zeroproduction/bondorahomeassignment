﻿using BondoraHomeAssignment.Server.Facades.Invoice;
using BondoraHomeAssignment.Server.Models.Loyalty;

namespace BondoraHomeAssignment.Server.Facades
{
    public class InvoiceFacade
    {
        public static CreateInvoice CreateInvoice(Models.Order.Orders order, LoyaltyPoint loyalityPoint) => new CreateInvoice(order, loyalityPoint);
    }
}
