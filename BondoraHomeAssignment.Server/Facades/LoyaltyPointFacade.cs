﻿namespace BondoraHomeAssignment.Server.Facades
{
    public class LoyaltyPointFacade
    {
        public static LoyaltyPoints.CalculateLoyaltyPoints LoyaltyPoints() => new LoyaltyPoints.CalculateLoyaltyPoints();
    }
}
