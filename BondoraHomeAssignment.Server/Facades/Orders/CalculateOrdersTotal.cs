﻿using System.Collections.Generic;
using System.Linq;
using BondoraHomeAssignment.Server.Interfaces.Order;
using BondoraHomeAssignment.Server.Models.Equipment;

namespace BondoraHomeAssignment.Server.Facades.Orders
{
    public class CalculateOrdersTotal
    {
        private readonly Models.Order.Orders _orders;

        private readonly Dictionary<EquipmentTypes.Types, IPriceCalculation> _orderEquipmentTypesFeesMapping = new Dictionary< EquipmentTypes.Types, IPriceCalculation>()
        {
            { EquipmentTypes.Types.Regular, new OrderRegularFee() },
            { EquipmentTypes.Types.Heavy, new OrderHeavyFee() },
            { EquipmentTypes.Types.Specilized, new OrderSpecilizedFee() },
        };

        public CalculateOrdersTotal(Models.Order.Orders orders)
        {
            _orders = orders;
        }

        public void CalculateTotal()
        {
            foreach (var order in _orders.OrdersList)
            {
                var fee = _orderEquipmentTypesFeesMapping.First(x => x.Key.Equals(order.Equipment.Type)).Value;
                order.Price = fee.Calculate(order);
                _orders.Total += order.Price;
            }
        }

        public int GetTotal() => _orders.Total;
        public Models.Order.Orders GetCalculatedOrders() => _orders;
    }
}
