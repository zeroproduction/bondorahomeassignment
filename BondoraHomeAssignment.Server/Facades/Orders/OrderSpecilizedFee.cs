﻿using BondoraHomeAssignment.Server.Interfaces.Order;
using BondoraHomeAssignment.Server.Models.Equipment;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Facades.Orders
{
    public class OrderSpecilizedFee : BaseOrder, IPriceCalculation
    {
        public int Calculate(Order order)
        {
            var priceTotal = 0;
            var firstThreeDays = 3;

            if (order.Dates <= firstThreeDays) priceTotal += EquipmentPrices.GetPriceByType(EquipmentPrices.PricesType.Premium);
            if (order.Dates > firstThreeDays)
            {
                var overDays = order.Dates - firstThreeDays;
                priceTotal += EquipmentPrices.GetPriceByType(EquipmentPrices.PricesType.Regular) * overDays;
            }

            return priceTotal;
        }
    }
}
