﻿using BondoraHomeAssignment.Server.Models.Equipment;

namespace BondoraHomeAssignment.Server.Facades.Orders
{
    public class BaseOrder
    {
        protected readonly EquipmentPrices EquipmentPrices = new EquipmentPrices();
    }
}
