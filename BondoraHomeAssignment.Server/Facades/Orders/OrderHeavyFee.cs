﻿using BondoraHomeAssignment.Server.Interfaces.Order;
using BondoraHomeAssignment.Server.Models.Equipment;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Facades.Orders
{
    public class OrderHeavyFee : BaseOrder, IPriceCalculation
    {
        public int Calculate(Order order)
        {
            return (EquipmentPrices.GetPriceByType(EquipmentPrices.PricesType.OneTime) + EquipmentPrices.GetPriceByType(EquipmentPrices.PricesType.Premium) * order.Dates);
        }
    }
}
