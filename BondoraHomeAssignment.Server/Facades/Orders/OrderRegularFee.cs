﻿using BondoraHomeAssignment.Server.Interfaces.Order;
using BondoraHomeAssignment.Server.Models.Equipment;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Facades.Orders
{
    public class OrderRegularFee : BaseOrder, IPriceCalculation
    {
        public int Calculate(Order order)
        {
            var firstTwoDays = 2;

            var priceTotal = EquipmentPrices.GetPriceByType(EquipmentPrices.PricesType.OneTime);

            if (order.Dates <= firstTwoDays)
            {
                priceTotal += EquipmentPrices.GetPriceByType(EquipmentPrices.PricesType.Premium) * firstTwoDays;
            }

            if (order.Dates > firstTwoDays)
            {
                priceTotal += EquipmentPrices.GetPriceByType(EquipmentPrices.PricesType.Premium) * (order.Dates - firstTwoDays);
            }

            return priceTotal;
        }
    }
}
