﻿using BondoraHomeAssignment.Server.Facades.TextFile;

namespace BondoraHomeAssignment.Server.Facades
{
    public class TextFileFacade
    {
        public static TextFileWrite TextFileWrite => new TextFileWrite();
    }
}
