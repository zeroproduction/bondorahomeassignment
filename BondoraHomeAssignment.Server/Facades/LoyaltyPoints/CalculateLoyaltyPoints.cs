﻿using BondoraHomeAssignment.Server.Models.Loyalty;

namespace BondoraHomeAssignment.Server.Facades.LoyaltyPoints
{
    public class CalculateLoyaltyPoints
    {
        private readonly LoyaltyPoint _loyaltyPoint = new LoyaltyPoint();
        private readonly Models.Loyalty.LoyaltyPoints _loyaltyPoints = new Models.Loyalty.LoyaltyPoints();

        public CalculateLoyaltyPoints Calculate(Models.Order.Orders orders)
        {
            foreach (var order in orders.OrdersList)
            {
                _loyaltyPoint.TotalPoints += _loyaltyPoints.GetLoyaltyPointByType(order.Equipment.Type);
            }

            return this;
        }

        public LoyaltyPoint GetLoyaltyPoint() => _loyaltyPoint;
    }
}
