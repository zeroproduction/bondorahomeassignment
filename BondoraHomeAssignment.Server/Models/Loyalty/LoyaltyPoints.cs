﻿using System.Collections.Generic;
using BondoraHomeAssignment.Server.Models.Equipment;

namespace BondoraHomeAssignment.Server.Models.Loyalty
{
    public class LoyaltyPoints
    {
        private Dictionary<EquipmentTypes.Types, int> PointsDictionary { get; } = new Dictionary<EquipmentTypes.Types, int>()
        {
            { EquipmentTypes.Types.Heavy, 2 },
            { EquipmentTypes.Types.Regular, 1 },
            { EquipmentTypes.Types.Specilized, 1 },
        };

        public int GetLoyaltyPointByType(EquipmentTypes.Types type)
        {
            return PointsDictionary[type];
        }
    }
}
