﻿namespace BondoraHomeAssignment.Server.Models.Loyalty
{
    public class LoyaltyPoint
    {
        public int TotalPoints { get; set; }

        public override string ToString()
        {
            return $"{TotalPoints}";
        }
    }
}
