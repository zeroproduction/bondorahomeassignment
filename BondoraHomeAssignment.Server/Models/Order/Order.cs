﻿using Newtonsoft.Json;

namespace BondoraHomeAssignment.Server.Models.Order
{
    public class Order
    {
        [JsonProperty("date")]
        public int Dates { get; set; }

        [JsonProperty("equipment")]
        public Equipment.Equipment Equipment { get; set; }

        public int Price { get; set; }

        public override string ToString()
        {
            return $"{Equipment.Name} {Price}€";
        }
    }
}
