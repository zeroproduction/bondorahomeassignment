﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BondoraHomeAssignment.Server.Models.Order
{
    public class Orders
    {
        [JsonProperty("orders_list")]
        public List<Order> OrdersList { get; set; }

        public int Total { get; set; }

        public override string ToString()
        {
            var ordersString = string.Empty;

            foreach (var order in OrdersList)
            {
                ordersString += $"{order} {Environment.NewLine}";
            }

            return ordersString;
        }
    }
}
