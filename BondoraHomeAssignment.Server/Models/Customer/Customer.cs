﻿using System.Collections.Generic;
using BondoraHomeAssignment.Server.Models.Loyalty;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Models.Customer
{
    public class Customer
    {
        public int CustomerId { get; private set; }
        public List<Orders> Orders { get; set; } = new List<Orders>();
        public LoyaltyPoint LoyaltyPoint { get; set; }

        public Customer()
        {
            CustomerId = Constants.CustomerId;
        }
    }
}
