﻿using Newtonsoft.Json;

namespace BondoraHomeAssignment.Server.Models.Equipment
{
    public class Equipment
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public EquipmentTypes.Types Type { get; set; }

        [JsonProperty("type_string")]
        public string TypeString { get => Type.ToString(); }
    }
}
