﻿using System.Collections.Generic;

namespace BondoraHomeAssignment.Server.Models.Equipment
{
    public class EquipmentPrices
    {
        public enum PricesType
        {
            OneTime, 
            Premium,
            Regular
        }

        public Dictionary<PricesType, int> PricesList = new Dictionary<PricesType, int>()
        {
            {PricesType.OneTime, 100},
            {PricesType.Premium, 60},
            {PricesType.Regular, 40}
        };

        public int GetPriceByType(PricesType pricesType) => PricesList[pricesType];
    }
}
