﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BondoraHomeAssignment.Server.Models.Equipment
{
    public class Equipments
    {
        [JsonProperty("equipments_list")]
        public List<Equipment> EquipmentsList = new List<Equipment>();
    }
}
