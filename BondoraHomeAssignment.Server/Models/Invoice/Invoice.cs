﻿using System;
using BondoraHomeAssignment.Server.Models.Loyalty;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Models.Invoice
{
    public class Invoice
    {
        public string Title { get; set; }
        public Orders Orders { get; set; }
        public LoyaltyPoint LoyaltyPoint { get; set; }
        public int TotalPrice { get; set; }

        public override string ToString()
        {
            return $"Title: {Title}{Environment.NewLine}{Environment.NewLine}" +
                   $"Orders:{Environment.NewLine}{Environment.NewLine}{Orders}{Environment.NewLine}" +
                   $"Summary:{Environment.NewLine}{Environment.NewLine}" +
                   $"Total: {TotalPrice}€{ Environment.NewLine}" +
                   $"Loyalty Points: {LoyaltyPoint}";
        }
    }
}
