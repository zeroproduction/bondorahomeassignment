﻿using System.Collections.Generic;
using BondoraHomeAssignment.Server.Models.Equipment;

namespace BondoraHomeAssignment.Server.Mock
{
    public class EquipmentListMockHelper
    {
        public static Equipments GetEquipmentList()
        {
            return new Equipments()
            {
                EquipmentsList = new List<Equipment>()
                {
                    new Equipment()
                    {
                        Name = "Caterpillar bulldozer",
                        Type = EquipmentTypes.Types.Heavy
                    },
                    new Equipment()
                    {
                        Name = "KamAZ truck",
                        Type = EquipmentTypes.Types.Regular
                    },
                    new Equipment()
                    {
                        Name = "Komatsu crane",
                        Type = EquipmentTypes.Types.Heavy
                    },
                    new Equipment()
                    {
                        Name = "Volvo steamroller",
                        Type = EquipmentTypes.Types.Heavy
                    },
                    new Equipment()
                    {
                        Name = "Bosch jackhammer",
                        Type = EquipmentTypes.Types.Specilized
                    },
                }
            };
        }
    }
}
