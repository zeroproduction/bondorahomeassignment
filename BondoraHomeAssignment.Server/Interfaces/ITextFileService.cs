﻿using BondoraHomeAssignment.Server.Models.Invoice;

namespace BondoraHomeAssignment.Server.Interfaces
{
    public interface ITextFileService
    {
        void SaveToTextFile(Invoice invoice);
    }
}
