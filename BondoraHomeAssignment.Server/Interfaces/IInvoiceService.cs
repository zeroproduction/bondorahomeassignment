﻿using BondoraHomeAssignment.Server.Events;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Interfaces
{
    public interface IInvoiceService
    {
        event CreateInvoiceTextFileEvent.CreateInvoiceTextEventHandler CreateInvoiceText;
        event CalculateOrderTotalsEvent.CalculateTotalsEventHandler CalculateTotals;
        event CalculateLoyaltyPointsEvents.CalculateLoyaltyPointsEventHandler CalculateLoyaltyPoints;
        bool GenerateInvoice(Orders order);
    }
}
