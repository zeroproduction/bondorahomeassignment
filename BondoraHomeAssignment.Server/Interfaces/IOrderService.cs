﻿using BondoraHomeAssignment.Server.Events;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Interfaces
{
    public interface IOrderService
    {
        void CalculateOrderTotals(Orders orders);
        event CacheEvents.UpdateCustomerOrdersEventHandler UpdateCustomerOrders;
    }
}
