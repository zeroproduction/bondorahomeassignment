﻿using BondoraHomeAssignment.Server.Events;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Interfaces
{
    public interface ILoyaltyPointService
    {
        void CalculateLoyaltyPoints(Orders order);
        event CacheEvents.UpdateCustomerLoyaltyPointsEventHandler UpdateCustomerLoyaltyPoints;
    }
}
