﻿using System;

namespace BondoraHomeAssignment.Server.Interfaces
{
    public interface IExceptionService
    {
        void HandleExceptions(Exception ex);
    }
}
