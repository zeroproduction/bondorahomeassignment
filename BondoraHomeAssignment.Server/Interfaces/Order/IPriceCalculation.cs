﻿namespace BondoraHomeAssignment.Server.Interfaces.Order
{
    public interface IPriceCalculation
    {
        int Calculate(Models.Order.Order order);
    }
}
