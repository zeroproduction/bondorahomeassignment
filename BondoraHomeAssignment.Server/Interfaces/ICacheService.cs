﻿using BondoraHomeAssignment.Server.Models.Customer;
using BondoraHomeAssignment.Server.Models.Loyalty;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Interfaces
{
    public interface ICacheService
    {
        void SaveCustomerToCache(Customer customer);
        Customer GetExistingCustomerById(int customerId);

        void UpdateCustomerOrders(Orders order, int customerId);
        void UpdateCustomerLoyalityPoints(LoyaltyPoint loyaltyPoint, int customerId);
    }
}
