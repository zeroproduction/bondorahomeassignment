﻿using System;
using BondoraHomeAssignment.Server.Events;
using BondoraHomeAssignment.Server.Facades;
using BondoraHomeAssignment.Server.Interfaces;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Services
{
    public class LoyaltyPointService : ILoyaltyPointService
    {
        private readonly IExceptionService _exceptionService;

        public event CacheEvents.UpdateCustomerLoyaltyPointsEventHandler UpdateCustomerLoyaltyPoints;

        public LoyaltyPointService(IExceptionService exceptionService)
        {
            _exceptionService = exceptionService;
        }

        public void CalculateLoyaltyPoints(Orders order)
        {
            try
            {
                var loyaltyPointFacade = LoyaltyPointFacade.LoyaltyPoints();
                UpdateCustomerLoyaltyPoints?.Invoke(this, loyaltyPointFacade.Calculate(order).GetLoyaltyPoint());
            }
            catch (Exception ex)
            {
                _exceptionService.HandleExceptions(ex);
            }
        }

        
    }
}
