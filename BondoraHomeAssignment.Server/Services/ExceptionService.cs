﻿using System;
using BondoraHomeAssignment.Server.Interfaces;

namespace BondoraHomeAssignment.Server.Services
{
    public class ExceptionService : IExceptionService
    {
        public void HandleExceptions(Exception ex)
        {
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
        }
    }
}
