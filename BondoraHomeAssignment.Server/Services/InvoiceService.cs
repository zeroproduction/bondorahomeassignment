﻿using System;
using BondoraHomeAssignment.Server.Events;
using BondoraHomeAssignment.Server.Facades;
using BondoraHomeAssignment.Server.Interfaces;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Services
{
    public class InvoiceService : IInvoiceService
    {
        private readonly ICacheService _cacheService;
        private readonly IExceptionService _exceptionService;

        public InvoiceService(IExceptionService exceptionService, ICacheService cacheService)
        {
            _cacheService = cacheService;
            _exceptionService = exceptionService;
        }
       
        public event CalculateOrderTotalsEvent.CalculateTotalsEventHandler CalculateTotals;
        public event CalculateLoyaltyPointsEvents.CalculateLoyaltyPointsEventHandler CalculateLoyaltyPoints;
        public event CreateInvoiceTextFileEvent.CreateInvoiceTextEventHandler CreateInvoiceText;

        public bool GenerateInvoice(Orders order)
        {
            try
            {
                CalculateTotals?.Invoke(this, order);
                CalculateLoyaltyPoints?.Invoke(this, order);
                var existingCustomer = _cacheService.GetExistingCustomerById(Constants.CustomerId);
                CreateInvoiceText?.Invoke(this, InvoiceFacade.CreateInvoice(order, existingCustomer.LoyaltyPoint).Generate());
                return true;
            }
            catch (Exception ex)
            {
                _exceptionService.HandleExceptions(ex);
                return false;
            }
        }
    }
}
