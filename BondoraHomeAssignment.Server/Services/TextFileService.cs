﻿using System;
using BondoraHomeAssignment.Server.Facades;
using BondoraHomeAssignment.Server.Interfaces;
using BondoraHomeAssignment.Server.Models.Invoice;

namespace BondoraHomeAssignment.Server.Services
{
    public class TextFileService : ITextFileService
    {
        private readonly IExceptionService _exceptionService;

        public TextFileService(IExceptionService exceptionService)
        {
            _exceptionService = exceptionService;
        }

        public void SaveToTextFile(Invoice invoice)
        {
            try
            {
                TextFileFacade.TextFileWrite.CreateFileWithContent(invoice);
            }
            catch (Exception ex)
            {
                _exceptionService.HandleExceptions(ex);
            }
        }
    }
}
