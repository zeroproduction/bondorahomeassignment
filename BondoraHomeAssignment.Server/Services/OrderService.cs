﻿using System;
using BondoraHomeAssignment.Server.Events;
using BondoraHomeAssignment.Server.Facades;
using BondoraHomeAssignment.Server.Interfaces;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignment.Server.Services
{
    public class OrderService : IOrderService
    {
        public event CacheEvents.UpdateCustomerOrdersEventHandler UpdateCustomerOrders;

        private readonly IExceptionService _exceptionService;

        public OrderService(IExceptionService exceptionService)
        {
            _exceptionService = exceptionService;
        }

        public void CalculateOrderTotals(Orders orders)
        {
            try
            {
                var calculatedOrder = OrderFacade.CalculateOrdersTotal(orders);

                calculatedOrder.CalculateTotal();
                var calculatedOrders = calculatedOrder.GetCalculatedOrders();
                UpdateCustomerOrders?.Invoke(this, calculatedOrders);
            }
            catch (Exception ex)
            {
                _exceptionService.HandleExceptions(ex);
            }
        }
    }
}
