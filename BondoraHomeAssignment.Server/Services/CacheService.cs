﻿using System;
using BondoraHomeAssignment.Server.Interfaces;
using BondoraHomeAssignment.Server.Models.Customer;
using BondoraHomeAssignment.Server.Models.Loyalty;
using BondoraHomeAssignment.Server.Models.Order;
using Microsoft.Extensions.Caching.Memory;

namespace BondoraHomeAssignment.Server.Services
{
    public class CacheService : ICacheService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IExceptionService _exceptionService;

        public CacheService(IMemoryCache memoryCache, IExceptionService exceptionService)
        {
            _memoryCache = memoryCache;
            _exceptionService = exceptionService;
        }
        public void SaveCustomerToCache(Customer customer)
        {
            try
            {
                _memoryCache.Set(customer.CustomerId, customer);
            }
            catch (Exception ex)
            {
                _exceptionService.HandleExceptions(ex);
            }
        }

        public Customer GetExistingCustomerById(int customerId)
        {
            return _memoryCache.TryGetValue(customerId, out Customer existingCustomer) ? existingCustomer : new Customer();
        }

        public void UpdateCustomerOrders(Orders order, int customerId)
        {
            try
            {
                var existingCustomer = GetExistingCustomerById(customerId);

                existingCustomer.Orders.Add(order);

                _memoryCache.Set(customerId, existingCustomer);
            }
            catch (Exception ex)
            {
                _exceptionService.HandleExceptions(ex);
            }
        }

        public void UpdateCustomerLoyalityPoints(LoyaltyPoint loyaltyPoint, int customerId)
        {
            try
            {
                var existingCustomer = GetExistingCustomerById(customerId);

                existingCustomer.LoyaltyPoint = loyaltyPoint;

                _memoryCache.Set(customerId, existingCustomer);
            }
            catch (Exception ex)
            {
                _exceptionService.HandleExceptions(ex);
            }
        }
    }
}
