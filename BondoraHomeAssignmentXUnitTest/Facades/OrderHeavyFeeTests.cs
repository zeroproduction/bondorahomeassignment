﻿using BondoraHomeAssignment.Server.Facades.Orders;
using BondoraHomeAssignment.Server.Models.Equipment;
using BondoraHomeAssignmentXUnitTest.Helper;
using Xunit;

namespace BondoraHomeAssignmentXUnitTest.Facades
{
    public class OrderHeavyFeeTests
    {
        [Fact]
        public void OrderHeavyFee_CalculatePrice_ReturnsPrice()
        {
            var orderHeavyFee = new OrderHeavyFee();
            var calculatedPrice = orderHeavyFee.Calculate(OrderTestData.GetSingleOrderWithEquipment(EquipmentTypes.Types.Heavy));

            Assert.Equal(220, calculatedPrice);
        }
    }
}
