﻿using BondoraHomeAssignment.Server.Facades.Orders;
using BondoraHomeAssignment.Server.Models.Equipment;
using BondoraHomeAssignmentXUnitTest.Helper;
using Xunit;

namespace BondoraHomeAssignmentXUnitTest.Facades
{
    public class OrderSpecializedFeeTests
    {
        [Fact]
        public void OrderRegularFee_CalculatePrice_ReturnsPrice()
        {
            var orderSpecilizedFee = new OrderSpecilizedFee();
            var calculatedPrice = orderSpecilizedFee.Calculate(OrderTestData.GetSingleOrderWithEquipment(EquipmentTypes.Types.Specilized, 4));

            Assert.Equal(40, calculatedPrice);
        }
    }
}
