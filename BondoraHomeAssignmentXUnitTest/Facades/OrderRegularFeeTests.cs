﻿
using BondoraHomeAssignment.Server.Facades.Orders;
using BondoraHomeAssignment.Server.Models.Equipment;
using BondoraHomeAssignmentXUnitTest.Helper;
using Xunit;

namespace BondoraHomeAssignmentXUnitTest.Facades
{
    public class OrderRegularFeeTests
    {
        [Fact]
        public void OrderRegularFee_CalculatePrice_ReturnsPrice()
        {
            var orderRegularFee = new OrderRegularFee();
            var calculatedPrice = orderRegularFee.Calculate(OrderTestData.GetSingleOrderWithEquipment(EquipmentTypes.Types.Regular, 4));

            Assert.Equal(220, calculatedPrice);
        }
    }
}
