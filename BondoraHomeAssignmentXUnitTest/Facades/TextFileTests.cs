﻿using BondoraHomeAssignment.Server.Facades;
using BondoraHomeAssignment.Server.Models.Invoice;
using BondoraHomeAssignmentXUnitTest.Helper;
using Xunit;

namespace BondoraHomeAssignmentXUnitTest.Facades
{
    public class TextFileTests
    {
        [Fact]
        public void TextFile_CreateFileWithContent_NotNull()
        {
            TextFileFacade.TextFileWrite.CreateFileWithContent(new Invoice()
            {
                Orders = OrderTestData.GetOrderWithTwoEquipments(),
                TotalPrice = 245,
                Title = "Invoice"
            });
        }
    }
}
