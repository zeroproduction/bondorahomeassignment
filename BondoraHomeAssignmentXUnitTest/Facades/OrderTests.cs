﻿using BondoraHomeAssignment.Server.Facades;
using BondoraHomeAssignmentXUnitTest.Helper;
using Xunit;

namespace BondoraHomeAssignmentXUnitTest.Facades
{
    public class OrderTests
    {
        [Fact]
        public void OrderFacade_CalculateOrder_ReturnNotZero()
        {
            var calculatedOrder = OrderFacade.CalculateOrdersTotal(OrderTestData.GetOrderWithTwoEquipments());
            calculatedOrder.CalculateTotal();
            Assert.Equal(440, calculatedOrder.GetTotal());
        }
    }
}
