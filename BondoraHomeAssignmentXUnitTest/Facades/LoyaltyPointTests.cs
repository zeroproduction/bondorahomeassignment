﻿using BondoraHomeAssignment.Server.Facades;
using BondoraHomeAssignmentXUnitTest.Helper;
using Xunit;

namespace BondoraHomeAssignmentXUnitTest.Facades
{
    public class LoyaltyPointTests
    {
        [Fact]
        public void LoyaltyPoint_CalculatePoints_ReturnTwo()
        {
            var loyaltyPointFacade = LoyaltyPointFacade.LoyaltyPoints();
            loyaltyPointFacade.Calculate(OrderTestData.GetSingleOrders());
            Assert.Equal(2, loyaltyPointFacade.GetLoyaltyPoint().TotalPoints);
        }

        [Fact]
        public void LoyaltyPoint_CalculatePoints_ReturnEight()
        {
            var loyaltyPointFacade = LoyaltyPointFacade.LoyaltyPoints();
            loyaltyPointFacade.Calculate(OrderTestData.GetEightOrderList());
            Assert.Equal(8, loyaltyPointFacade.GetLoyaltyPoint().TotalPoints);
        }
    }
}
