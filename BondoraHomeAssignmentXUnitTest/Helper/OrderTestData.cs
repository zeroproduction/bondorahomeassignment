﻿using System.Collections.Generic;
using BondoraHomeAssignment.Server.Models.Equipment;
using BondoraHomeAssignment.Server.Models.Order;

namespace BondoraHomeAssignmentXUnitTest.Helper
{
    public class OrderTestData
    {
        public static Orders GetOrderWithTwoEquipments()
        {
            return new Orders()
            {
                OrdersList = new List<Order>()
                {
                    new Order()
                    {
                        Equipment = new Equipment()
                        {
                            Type = EquipmentTypes.Types.Heavy
                        },
                        Dates = 2
                    },
                    new Order()
                    {
                        Equipment = new Equipment()
                        {
                            Type = EquipmentTypes.Types.Regular
                        },
                        Dates = 2
                    }
                }
            };
        }

        public static Order GetSingleOrderWithEquipment(EquipmentTypes.Types type, int days = 2)
        {
            return new Order()
            {
                Equipment = new Equipment()
                {
                    Type = type
                },
                Dates = days
            };
        }

        public static Orders GetEightOrderList()
        {
            return new Orders()
            {
                OrdersList = new List<Order>()
                {
                    new Order()
                    {
                        Equipment = new Equipment()
                        {
                            Type = EquipmentTypes.Types.Heavy
                        },
                        Dates = 2
                    },
                    new Order()
                    {
                        Equipment = new Equipment()
                        {
                            Type = EquipmentTypes.Types.Regular
                        },
                        Dates = 2
                    },
                    new Order()
                    {
                        Equipment = new Equipment()
                        {
                            Type = EquipmentTypes.Types.Regular
                        },
                        Dates = 2
                    },
                    new Order()
                    {
                        Equipment = new Equipment()
                        {
                            Type = EquipmentTypes.Types.Heavy
                        },
                        Dates = 2
                    },
                    new Order()
                    {
                        Equipment = new Equipment()
                        {
                            Type = EquipmentTypes.Types.Heavy
                        },
                        Dates = 2
                    }
                }
            };
        }

        public static Orders GetSingleOrders()
        {
            return new Orders()
            {
                OrdersList = new List<Order>()
                {
                    new Order()
                    {
                        Equipment = new Equipment()
                        {
                            Type = EquipmentTypes.Types.Heavy
                        },
                        Dates = 2
                    }
                }
            };
        }
    }
}
